create table AccountInfo(
AccountId	int identity(1,1) not null,		--账户编号
AccountCode	varchar(20) not null,		--身份证号码
AccountPhone	varchar(20),		--电话号码
RealName	varchar(20) not null,	--真实姓名
);
create table  BankCard(
CardNo	varchar(30)primary key,	--银行卡号
AccountId	int not null,		--账户编号
CardPwd	varchar(30) not null, 	--银行密码
CardBalance	money not null, 		--银行卡余额
CardState	tinyint	 not null, 	--银行卡状态：1正常；2挂失；3冻结；4注销；5睡眠；
CardTime	varchar(30)	not null, 	--开卡时间
);
create table  CardExchange(
ExchangeId	int  identity(1,1) primary key,--交易编号
CardNo	varchar(30) not null,	--银行卡号
MoneyInBank	money not null,	--存钱金额
MoneyOutBank	money not null,	--取钱金额
ExchangeTime	smalldatetime not null,		--交易时间
);
alter table AccountInfo  add OpenTime smalldatetime  not null  ;
alter table  BankCard  alter column CardTime	smalldatetime not null ;
alter table BankCard add constraint DF_BankCard_CardTime default (getdate()) for CardTime ;
alter table AccountInfo add constraint	PK_AccountInfo_AccountId  primary key (AccountId );
alter table AccountInfo add constraint  UNQ_AccountInfo_AccountCode unique(AccountCode);
alter table AccountInfo add constraint	DF_AccountInfo_OpenTime default (getdate()) for OpenTime ;
alter table  AccountInfo  alter column  AccountPhone varchar(20)  not null ;
alter table BankCard  add constraint	DF_BankCard_CardBalance  default (0.00) for CardBalance ;
alter table BankCard  add constraint	DF_BankCard_CardState  default (1) for CardState ;
alter table BankCard add constraint FK_BankCard_AccountId foreign key (AccountId) references AccountInfo(AccountId );
alter table CardExchange add constraint FK_CardExchange_CardNo foreign key (CardNo) references BankCard(CardNo);
alter table CardExchange add constraint CK_CardExchange_MoneyInBank check(MoneyInBank >=0);
alter table CardExchange add constraint CK_CardExchange_MoneyOutBank  check(MoneyOutBank >=0);

