package 图书管理系统;

import java.util.Scanner;

public class 图书管理系统 {
	static String[][] user = new String[100][4];
	static String[][] book = new String[100][5];
	static String PubCom[][]=new String[100][3];
	
	public static  void us() {
		user[0][0] = "HP001";user[0][1] = "ken";  user[0][2] ="123"; user[0][3] ="admin";
		user[1][0] = "HP001";user[1][1] = "ken";  user[1][2] ="123"; user[1][3] ="admin";
		user[2][0] = "HP003";user[2][1] = "guile";user[2][2] = "123";user[2][3] = "user";
	}
	public static void bookone(){
		
		book[0][0] = "001";book[0][1] = "JAVA高级编程";book[0][2] = "100";	book[0][3] = "铁道部出版社";book[0][4] = "张三";			
		book[1][0] = "002";book[1][1] = "SQLSERVER高级编程";book[1][2] = "150";book[1][3] = "清华出版社";book[1][4] = "李四";
		book[2][0] = "003";book[2][1] = "JAVA基础入门";book[2][2] = "50";book[2][3] = "铁道部出版社";book[2][4] = "张三";
		book[3][0] = "004";book[3][1] = "ORACLE高级编程";book[3][2] = "120";book[3][3] = "清华出版社";book[3][4] = "王五";
	}
	//出版社信息
	public static void administartor2() {
		
		PubCom[0][0]="清华出版社"; PubCom[0][1]="武汉市XXX"; PubCom[0][2]="aaa";
		PubCom[1][0]="铁道出版社"; PubCom[1][1]="北京市XXX"; PubCom[1][2]="bbb";
		PubCom[2][0]="邮电出版社"; PubCom[2][1]="南京市XXX"; PubCom[2][2]="ccc";
	}
	public static void BookAdd() {
		Scanner scanner=new Scanner(System.in);
		System.out.println("请输入图书的编号");
		String isbn = scanner.next();
		System.out.println("请输入图书的名称");
		String bookName = scanner.next();
		System.out.println("请输入图书的价格");
		String price = scanner.next();
		System.out.println("请输入图书的出版社");
		String pub = scanner.next();
		System.out.println("请输入图书的作者");
		String author = scanner.next();
		
		int index=getIndex(isbn);
		//如果index等于-1,不存在要增加的书籍信息，进行增加操作
		if(index==-1) {
			//查找书籍数组book中第一个为null的行下标
			int i=getindexnull();
			
			//如果i不等于-1,在book数组中找到没有存放书籍信息的数组行下标i
			if(i!=-1) {
				//将用户输入的书籍信息存放到book数组的第i行元素中
				book[i][0] = isbn;
				book[i][1] = bookName; 
				book[i][2] = price;
				book[i][3] = pub;
				book[i][4] = author;
				printallBook();
				System.out.println("增加成功");
			}else {
				System.out.println("书籍已满，无法添加");
			}
		}else {
			System.out.println("书籍信息已存在，添加失败");
		}
	}
	//输出书籍信息
	public static void printallBook() {
		System.out.println("书籍isbn \t 书籍名称 \t 书籍价格 \t 出版社 \t 作者");
		for (int i = 0; i < book.length; i++) {
			if (book[i] != null && book[i][0]!=null) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.println(book[i][j]);
				}
				System.out.println();
			}
		}
	}
	//查询下标是否为空 null
	public static int getindexnull() {
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]==null) {
				index=i;
				break;
			}
			
		}
		return index;
	}
	//查找编码所属的书籍下标;
	public static int getIndex(String isbn) {
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			if (isbn.equals(book[i][0])) {
				index=i;
				break;
			}
		}
		return index;
	}
	//删除书籍信息
	public static void BookDe() {
		System.out.println("请输入要删除的isbn编码：");
		Scanner sc=new Scanner(System.in);
		String isbn=sc.next();
		
		int Del=getIndex(isbn);
		if (Del == -1) {
			System.out.println("没有找到要删除的书籍信息");
		}else {
			book[Del][0] = null;
			book[Del][1] = null;
			book[Del][2] = null;
			book[Del][3] = null;
			book[Del][4] = null;
			printallBook();
		}
	}
	//更新书籍信息
	public static void Bookupdate() {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入要更新的书籍的isbn编码：");
		String isbn = sc.next();
		
		System.out.println("请输入更新后的图书名称");
		String bookName = sc.next();
		System.out.println("请输入更新后的图书价格");
		String price = sc.next();
		
		System.out.println("请输入更新后的图书出版社");
		String pub = sc.next();
		System.out.println("请输入更新后的图书作者");
		String author = sc.next();
		
		int index = getIndex(isbn);
		
		if(index != -1) {
			//调用方法将数据保存到数组中的第index行
			book[index][0] = isbn;
			book[index][1] = bookName; 
			book[index][2] = price;
			book[index][3] = pub;
			book[index][4] = author;
		}
		else {
			System.out.println("没有找到要修改的isbn编码的书籍");
		}
	}
	//查询书籍信息
	public static void Bookselect() {
		while(true) {
			System.out.println("1根据ISBN查询 2根据书名查询（模糊）3根据出版社查询 4根据作者查询 5根据价格范围查询 6查询所有书籍信息 7返回上一级菜单");
			Scanner sc=new Scanner(System.in);
			System.out.println("请输入查好的功能");
			int c=sc.nextInt();
				if (c==1) {
					System.out.println("请输入需要查找的书籍编码");
					String isbn=sc.next();
					
					select(isbn);
				}else if (c==2) {
					
					Scanner ssc=new Scanner(System.in);
					System.out.println("请输入要查询的书籍信息");
					
					String bookname=ssc.next();
					selectbook(bookname);
					
				}else if (c==3) {
					selectpuc();
				}else if (c==4) {
					selectpucc();
				}else if (c==5) {
					selectprice();
				}else if (c==6) {
					selectbookname();
				}else if (c==7) {
					break;
				}else {
					System.out.println("请重新输入");
				}
		}
	}
	//根据ISBN查询
	public static void select(String isbn) {
		int index=getIndex(isbn);
		System.out.println("书籍isbn\t书籍名称\t书籍价格\t出版社\t作者");
		for (int i = 0; i < book[index].length; i++) {
			System.out.println(book[index][i]+"\t");
		}
		System.out.println();
	}
	//根据书名模糊查询
	public static void selectbook(String bookname) {
		for (int i = 0; i < book.length; i++) {
			if (book[i][1]!=null && book[i][1].indexOf(bookname)!=-1) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.println(book[i][j]+"\t");
				}
				System.out.println();
			}
		}
	}
	//根据出版社查询
	public static void selectpuc() {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入需要查找的书籍编码");
		String aa=sc.next();
		pubselect(aa);
	} 
	//根据作者查询
	public static void selectpucc() {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入作者");
		String a=sc.next();
		pubzz(a);
	}
	//根据价格范围查询
	public static void selectprice() {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入最大范围");
		double min=sc.nextDouble();
		System.out.println("请输入最小范围");
		double max=sc.nextDouble();
		
			for (int i = 0; i < book.length; i++) {
				if (book[i][2] !=null) {
					double hh=Double.parseDouble(book[i][2]);
					if (hh<=max&&hh>=min) {
						for (int j = 0; j < book.length; j++) {
							System.out.println(book[i][j]+"\t");
						}
						System.out.println();
					}
				}
			}
	}
	//查询所有书籍信息
	public static void selectbookname() {
		printallBook();
	}
	//根据作者查询
	public static void pubzz(String a) {
		
		for (int i = 0; i < book.length; i++) {
			if (a.equals(book[i][4])) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4] + "\t");
			}
		}
	}
	public static void booktwo() {
		bookone();
		while(true) {
			
			Scanner sc=new Scanner(System.in);
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			int a1=sc.nextInt();
				if(a1==1) {
					BookAdd();
				}else if(a1==2) {
					BookDe();	
				}else if(a1==3) {
					Bookupdate();	
				}else if(a1==4) {
					Bookselect();
				}else if(a1==5) {
					break;
				}else {
					System.out.println("输入错误，请重新输入");
				}
		
		}	
	}
	//添加出版社信息
			public static void PubAdd() {
				administartor2();
				Scanner sc=new Scanner(System.in);
				System.out.println("请输入出版社信息");
				String a=sc.next();
				System.out.println("请输入出版社地址");
				String b=sc.next();
				System.out.println("请输入联系人");
				String c=sc.next();
				//遍历下标
				int arr=puc(a);
				
				if (arr==-1) {
					
					int i=pucnull();
					if (i!=-1) {
						 
							PubCom[i][0]=a;//出版社
							PubCom[i][1]=b;//出版社地址
							PubCom[i][2]=c;//联系人
							System.out.println("添加成功");
							printpuc();
						
					}else {
						System.err.println("出版社已满，无法添加");
					}
				}else {
					System.err.println("出版社已存在，添加失败");
				}
			}
			//输出出版社
			public static void printpuc() {
				System.out.println("出版社 \t 出版社地址 \t 联系人");
				for (int i = 0; i < PubCom.length; i++) {
					if (PubCom[i] != null && PubCom[i][0]!=null) {
						for (int j = 0; j < PubCom[i].length; j++) {
								System.out.println(PubCom[i][j]);
						}
						System.out.println();
					}else {
						break;
					}
				}
			}
			//判读下标是否为空
			public static int pucnull() {
				int arr=-1;
					for (int i = 0; i < PubCom.length; i++) {
						if (PubCom[i][0]==null) {
							arr=i;
							break;
						}
					}
				return arr;
				
			}
			//查找书籍下标信息
			public static int puc(String a) {
				int arr=-1;
					for (int i = 0; i < PubCom.length; i++) {
						if (a.equals(PubCom[i][0])) {
							arr=i;
							break;
						}
					}
				return arr;
			}
			//删除出版社信息
			public static void pucdel() {
				System.out.println("请输入要删除的出版社名称：");
				Scanner sc=new Scanner(System.in);
				String a=sc.next();
				
				int Del=puc(a);
				if (Del == -1) {
					System.out.println("没有找到要删除的书籍信息");
				}else {
					PubCom[Del][0] = null;
					PubCom[Del][1] = null;
					PubCom[Del][2] = null;
					printpuc();
				}
			}
			//更新出版社信息
			public static void pucupdate() {
				Scanner sc=new Scanner(System.in);
				System.out.println("请输入要更新的出版社编码：");
				String a=sc.next();
				System.out.println("请输入要更新的出版社地址编码：");
				String b=sc.next();
				System.out.println("请输入要更新的联系人编码：");
				String c=sc.next();
				
				int arr=puc(a);
				if (arr!=-1) {
					PubCom[arr][0]=a;//出版社
					PubCom[arr][1]=b;//出版社地址
					PubCom[arr][2]=c;//联系人
				}else {
					System.out.println("没有找到你要更新的出版社信息");
				}
				
			}
			//根据出版社名称查询
			public static void pubselect(String a) {
				int arr=puc(a);
				System.out.println("出版社 \t 出版社地址 \t 联系人");
				for (int i = 0; i < PubCom[arr].length; i++) {
					System.out.println(PubCom[arr][i]+"\t");
				}
				System.out.println();
			}
			//出版社管理系统
			public static void puc() {
				while(true) {
				Scanner sc=new Scanner(System.in);
				System.out.println("请先添加或者删除在进行查询操作");
				System.out.println("1 增加 2删除 3更新 4根据出版社名称查询 5查询所有出版社 6返回上一级菜单");
				int a=sc.nextInt();
				if (a==1) {//增加
					PubAdd();
				}else if(a==2) {//删除
					pucdel();
				}else if (a==3) {//更新
					pucupdate();
				}else if (a==4) {//根据出版社名称查询
					System.out.println("请输入需要查找的书籍编码");
					String aa=sc.next();
					pubselect(aa);
				}else if (a==5) {//查询所有出版社
					printpuc();
				}else if (a==6) {//返回上一级菜单
					break;
				}else {
					System.out.println("输入错误");
				}
					}
			}

	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		us();
		boolean login = true;
		while(login) {
			System.out.println("**********欢迎使用图书管理系统*************");
			System.out.println("请输入账号：");
			String username = scanner.next();
			System.out.println("请输入密码：");
			String password = scanner.next();
			boolean find = false;
			for (int i = 0; i < user.length; i++) {
				if(username.equals(user[i][1])&&password.equals(user[i][2])) {
					find=true;
					break;
				}
			}
			if(find) {
				System.out.println("登录成功");
				while(true) {
					System.out.println("1 图书管理  2 出版社管理  3 退出系统");
					System.out.println("请输入数字选择功能菜单：");
					Scanner sc=new Scanner(System.in);
					int a=sc.nextInt();
					if(a==1) {
						booktwo();
					}else if(a==2) {
						puc();
					}else if(a==3) {
						System.exit(0);
					}else {
						System.out.println("重新选择");
					}
				}
			}
			else {
				System.out.println("登陆失败");
			}
		}
		
	}
	
}


